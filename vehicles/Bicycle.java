//2133677
package vehicles;

public class Bicycle{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle(String manufacturer, int numberGears, double maxSpeed){
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    public double getmaxSpeed(){
        return this.maxSpeed;
    }

    public String getManufacturer(){
        return this.manufacturer;
    }
    
    public int getnumberGears(){
        return this.numberGears;
    }

    public String toString(){
        String bPrint = "Manufacturer: " + this.manufacturer + ", Number of Gears: " + this.numberGears + ", Max Speed: " + this.maxSpeed;
        return bPrint;
    }
}