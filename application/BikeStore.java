//2133677

package application;
import vehicles.*;

public class BikeStore{
    public static void main(String[]args){
        Bicycle[] inventory = new Bicycle[4];

        inventory[0] = new Bicycle("Panorama", 7, 65.0);
        inventory[1] = new Bicycle("T-Lab", 12, 75.0);
        inventory[2] = new Bicycle("KindHuman", 14, 125.0);
        inventory[3] = new Bicycle("LandYachtz", 6, 80.0);

        for(int i = 0; i < inventory.length; i++){
            System.out.println(inventory[i]);
        }
    }
}